﻿Shader "YETi/VR_NormalMappedSpecular" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalTex ("Normal map", 2D) = "blue" {}
		_SpecularTex ("Spec Map", 2D) = "gray" {}

		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.03, 16)) = 2.0
		_GlossValue ("GlossValue", Range (0.0, 16.0)) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong

		sampler2D _MainTex;
		sampler2D _NormalTex;
		sampler2D _SpecularTex;

		float _Shininess;
		float _GlossValue;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = UnpackNormal( tex2D ( _NormalTex, IN.uv_MainTex ) );

			float4 f4SpecularColor = tex2D( _SpecularTex, IN.uv_MainTex );
			o.Specular = _Shininess;
			o.Gloss = _GlossValue * f4SpecularColor.r;
		}
		ENDCG
	} 
	FallBack "Specular"
}
