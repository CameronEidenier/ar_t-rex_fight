﻿using UnityEngine;
using System.Collections;

public class ThunderEmitter : MonoBehaviour {

	float timer;
	public float max = 5;
	public float min = 1;
	float timeToHit = 0;

	public AudioClip clip1;
	public AudioClip clip2;
	AudioSource player;
	// Use this for initialization
	void Start () 
	{
		player = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer += Time.deltaTime;

		if (timer > timeToHit)
		{
			timer = 0;
			timeToHit = Random.Range(min, max);
			if (Random.value < .5f)
			{
			 	player.clip = clip1;
			}
			else
				player.clip = clip2;

			player.pitch = Random.Range(.9f, 1.1f);
			player.Play();

		}
	}
}
