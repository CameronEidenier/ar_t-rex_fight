﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Nodes : MonoBehaviour 
{
	public List<GameObject> nodes = new List<GameObject>();

	void Start()
	{
		//set each node id to be its array number
		for (int i = 0; i < nodes.Count; i++)
		{
			nodes [i].GetComponent<Node> ().id = i;

		}
	}

	public void RemoveDeadNodes()
	{
		for (int i = 0; i < nodes.Count; i++)
		{
			if (nodes[i] != null)
				nodes[i].GetComponent<Node>().RemoveDeadNeighbors();
		}


		for (int i = 0; i < nodes.Count; i++)
		{
			if (nodes[i] == null)
			{
				nodes.RemoveAt(i);
				i--;
			}
		}

	}

}
