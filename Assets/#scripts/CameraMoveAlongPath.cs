﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraMoveAlongPath : MonoBehaviour {

	bool mouseDown = false;

	private CardboardHead head;
	public float speed = 1f;
	public float maxViewingAngleHorz = 5f;
	public float maxViewingAngleVert = .5f;
	public float closeViewingAngle = 10f;
	public float playerTweenDistance = 1.5f;
	private Vector3 selectedDestinationPos;
	private GameObject targetNode;
	private List<GameObject> nodes;
	public GameObject nodesObject;
	private int selectedNode;
	private int previousNode;
	private bool atNode = false;
	private bool closeToNode = false;

	bool animating = false;
	bool isStoppedAtNode = false;
	bool isMoving = false;
	public bool enabled = false;


	public bool circleMovement = true;

	// Use this for initialization
	void Start () 
	{
		head =  Camera.main.GetComponent<StereoController>().Head;

		nodes = nodesObject.GetComponent<Nodes> ().nodes;
		//move head to node
		transform.position = new Vector3(nodes[0].transform.FindChild("TriggerHolder").position.x, transform.position.y, nodes[0].transform.FindChild("TriggerHolder").position.z);

		previousNode = 0;

		foreach (GameObject n in nodes) 
		{
			n.GetComponent<Node> ().hide ();
			n.GetComponent<Node> ().enabled = false;

		}

		//set target info
		targetNode = nodes [1].gameObject;
		selectedDestinationPos = targetNode.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Get mouse state
		if (Input.GetMouseButtonDown (0)) 
		{
			mouseDown = true;
		}
		else if (Input.GetMouseButtonUp (0)) 
		{
			mouseDown = false;
		}

		//set destination to last node looked at
		if (IsLookingAtTrigger() ) 
		{
			//set destination to selected node
			selectedDestinationPos = targetNode.transform.position;

			//check to see if target node is not the same as the previous node before setting new target node
			if(previousNode != targetNode.GetComponent<Node> ().id)
				selectedNode = targetNode.GetComponent<Node> ().id;
		}

		//check if close to target node, this is to adjust viewing angle
		//TODO look into using math to always adjust the viewing angle 
		if (targetNode != null) 
		{
			if (Vector2.Distance (new Vector2 (head.transform.position.x, head.transform.position.z), new Vector2 (targetNode.transform.position.x, targetNode.transform.position.z)) < 5f) 
			{
				closeToNode = true;
			}
		}

		//move player if mouse is down and they are looking at a Node
		if (targetNode.GetComponent<Node>().hasLookedForTime && CheckIfLookingForward ()) {
			//move camera
			transform.position += new Vector3 (head.transform.forward.x, 0, head.transform.forward.z) * speed * Time.deltaTime;
			isMoving = true;
			isStoppedAtNode = false;
		} 

		if (!CheckIfLookingForward ()) 
		{
			isMoving = false;
			targetNode.GetComponent<Node> ().lookingAtWP = false;
			targetNode.GetComponent<Node> ().hasLookedForTime = false;

		}
//		if (!mouseDown && isMoving && !animating && CheckIfLookingForward ())
//		{
//			
//			transform.position += new Vector3(head.transform.forward.x, 0, head.transform.forward.z) * speed * Time.deltaTime;
//			isStoppedAtNode = false;
//		}

		//update node behavior
		UpdateNodes ();

		//movee player if close to target node
		if (targetNode != null) 
		{
			if (Vector2.Distance (new Vector2 (head.transform.position.x, head.transform.position.z), new Vector2 (targetNode.transform.position.x, targetNode.transform.position.z)) < playerTweenDistance &&
				!animating && !isStoppedAtNode) 
			{
				//Debug.Log("Tween");
				iTween.MoveTo (gameObject, iTween.Hash ("position", new Vector3 (targetNode.transform.position.x, transform.position.y, targetNode.transform.position.z), "time", .3f, "easetype", "linear", "oncomplete", "StopAnimating"));
				//make user need to click on next node to move
				animating = true;
			}
		}
	}

	public bool CheckIfLookingForward()
	{
		return CheckIfLookingForward (maxViewingAngleHorz);
	}

	public bool CheckIfLookingForward(float horizontalAngle)
	{
		//set destination Vector3 to proper position regarding camera position
		Vector3 destination = selectedDestinationPos - transform.position;

		//check to see if viewing angle is correct
		if (Vector2.Angle (new Vector2 (destination.x, destination.z), new Vector2 (head.transform.forward.x, head.transform.forward.z)) < horizontalAngle
		    && head.transform.forward.y <= maxViewingAngleVert && head.transform.forward.y >= -maxViewingAngleVert)
			return true;
		if (closeToNode
			&& Vector2.Angle (new Vector2 (destination.x, destination.z), new Vector2 (head.transform.forward.x, head.transform.forward.z)) < closeViewingAngle 
			&& head.transform.forward.y <= maxViewingAngleVert && head.transform.forward.y >= -closeViewingAngle) 
		{
			return true;
		}
		
		else 
		{
			return false;
		}
	}

	public bool IsLookingAtTrigger()
	{
		RaycastHit hit;
		int layerMask = 1 << 8; //trigger layer
		if (Physics.Raycast (head.Gaze, out hit, 100, layerMask)) 
		{
			targetNode = hit.transform.gameObject.transform.parent.transform.parent.gameObject;
			//targetNode.GetComponent<Node>().moveTriggerAway(head.transform.position);


			targetNode.GetComponent<Node> ().lookingAtWP = true;

			
			if(targetNode.GetComponent<Node>().enabled)
				return true;
		}
		return false;
	}

	void StopAnimating()
	{
		isStoppedAtNode = true;
		animating = false;
		isMoving = false;

	}

	/*
	UpdateNodes updates all the nodes in the scene and does the following:
		-check to see if player is at a node
		-turns off all nodes
		-turns on nodes adjacent to player
		-checks to see if a node is being looked at, and needs to glow
	*/
	void UpdateNodes()
	{
		if (!enabled)
			return;

		//check to see if you are at a node
		foreach (GameObject n in nodes) 
		{
			Node node = n.GetComponent<Node> ();
			//check if at a node
			atNode = false;
			float dist = Vector2.Distance (new Vector2 (head.transform.position.x, head.transform.position.z), new Vector2 (n.transform.position.x, n.transform.position.z));
			if (dist < 1) 
			{
				previousNode = node.id;
				atNode = true;
				closeToNode = false;
			} 
			if (atNode) 
				break;
			

		}
		//loop through again to turn them all off
		foreach (GameObject n in nodes) 
		{
			n.GetComponent<Node> ().ToggleEnabled (false);
			if (targetNode != null) 
			{
				if (targetNode.GetComponent<Node> ().id != n.GetComponent<Node> ().id)
					n.GetComponent<Node> ().ToggleGlow (false);
			}
		}
		//if player is at a node, toggle nodes left and right to be active
		if (atNode) 
		{
			nodes [previousNode].GetComponent<Node> ().ToggleNeighbors (true);
			nodes [previousNode].GetComponent<Node> ().ToggleLight (true);
			nodes [previousNode].GetComponent<Node> ().stopProgress();

		} else if (circleMovement) { 
			//if player is between nodes, set only previous node and the node they clicked on to be active
			nodes [previousNode].GetComponent<Node> ().ToggleEnabled (true);
			nodes [selectedNode].GetComponent<Node> ().ToggleEnabled (true);
			nodes [previousNode].GetComponent<Node> ().ToggleLight (false);
		} else {
			nodes [previousNode].GetComponent<Node> ().ToggleNeighbors (true);
			nodes [previousNode].GetComponent<Node> ().ToggleEnabled (true);
			nodes [previousNode].GetComponent<Node> ().ToggleLight (false);
		}

		//set target node to glow if being looked at
		if (CheckIfLookingForward()) 
		{
			if (targetNode.GetComponent<Node>().enabled)
				targetNode.GetComponent<Node> ().ToggleGlow (true);
			//targetNode.GetComponent<Node>().moveTriggerAway(head.transform.position);
		} 
		else 
		{
			if (targetNode != null) 
			{
				//targetNode.GetComponent<Node> ().moveTriggerToOrigin ();
				targetNode.GetComponent<Node> ().ToggleGlow (false);
			}
		}
	}
}
