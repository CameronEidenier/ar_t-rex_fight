﻿using UnityEngine;
using System.Collections;

public class DoorHandler : MonoBehaviour {

	Animator leftDoor;
	Animator rightDoor;
	Node waypoint;
	CameraMoveAlongPath player;
	TrexRoar trex;

	public AudioClip openAudio;
	public AudioClip closeAudio;
	 AudioSource audioPlayer;
	bool doorsClosed = false;
	// Use this for initialization
	void Start () 
	{
		leftDoor = GameObject.Find("_PILLARLEFT").GetComponent<Animator>();
		rightDoor = GameObject.Find("_PILLARRIGHT").GetComponent<Animator>();
		waypoint = GameObject.Find("_ENDWAYPOINT").GetComponent<Node>();
		player = GameObject.Find("_PLAYER").GetComponent<CameraMoveAlongPath>();
		trex = GameObject.Find("_TREX").GetComponent<TrexRoar>();

		audioPlayer = GetComponent<AudioSource>();

		trex.Disable();
		waypoint.ToggleEnabled(false);
		waypoint.hide();
		Invoke("OpenGates", 2f);
	}

	void OpenGates()
	{
		Debug.Log("Open");
		leftDoor.Play("DoorOpen");
		rightDoor.Play("DoorOpen");

		audioPlayer.clip = openAudio;
		audioPlayer.Play();
		Invoke("EnableWaypoint", 4f);
	}

	void EnableWaypoint()
	{
		waypoint.ToggleEnabled(true);
		player.enabled = true;

	}

	void CloseGates()
	{
		
		audioPlayer.clip = closeAudio;
		audioPlayer.Play();
		leftDoor.Play("DoorClose");

		rightDoor.Play("DoorClose");
		Invoke("StartTrex", .1f);
	}

	void StartTrex()
	{	
		trex.Begin();
	}

	void OnTriggerEnter(Collider other)
	{
		if (doorsClosed)
			return;

		doorsClosed = true;
		CloseGates();
	}
	
	// Update is called once per frame

	void Update () 
	{
		
	}
}
