﻿using UnityEngine;
using System.Collections;

public class CameraLookAt : MonoBehaviour {

	public GameObject trex;

	private CardboardHead head;
	 

	void Start () 
	{
		head =  Camera.main.GetComponent<StereoController>().Head;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		//Ray ray = main.ScreenPointToRay(new  Vector3(main.pixelWidth / 2, main.pixelHeight / 2, 0));
		RaycastHit hit;
		// create a plane at 0,0,0 whose normal points to +Y:

		//Plane hPlane = new Plane (Vector3.up, Vector3.zero);
	
		//float distance = 0; 
		//Use this if you want to detect where it hits on a flat plane at 0,0,0
//		if (hPlane.Raycast(ray, out distance))
//		{
//			obj.transform.position = ray.GetPoint(distance); 
//
//		}
		//int layerMask = 1 << 8; //Terrain layer
		//Use this if you want to use terrain as the collider, such that the target moves up slopes
		if (Physics.Raycast (head.Gaze, out hit, 1000)) 
		{
			//obj.transform.position = hit.point;
			if (hit.transform.name == "BoxTrigger")
			{
				trex.GetComponent<TrexPathFollow>().StartEnterPath();
			}

		}



		//Ray ray = new Ray()
	}

}
