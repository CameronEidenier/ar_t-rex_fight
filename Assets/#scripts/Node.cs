﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


[Serializable]
public class Node : MonoBehaviour 
{

	public int id;
	public bool enabled = true;
	private bool animating = false;
	private bool lightOn = false;
	private bool adjustingLight = false;
	private Light lt;
	public float lightIntensity = 5;
	private Vector3 originalTriggerPos;
	private bool movedTrigger = false;
	private CardboardHead head;

	public bool hasLookedForTime = false;
	public bool lookingAtWP = false;
	float timeToLook = 1;



	[SerializeField]
	public List<Node> neighbors = new List<Node>();

	void onStart()
	{
		
	}

	void Update()
	{
		if (lookingAtWP && !hasLookedForTime) 
		{
			if (enabled)
				TweenProgressUpdate (Time.deltaTime / timeToLook);
		} 
		else 
		{
			transform.FindChild ("TriggerHolder/WaypointTrigger/Canvas/Progress").GetComponent<Image> ().fillAmount = 0;
			//hasLookedForTime = false;
		}
	}



	public void RemoveDeadNeighbors()
	{
		for (int i = 0; i < neighbors.Count; i++)
		{
			if (neighbors[i] == null)
			{
				neighbors.RemoveAt(i);
				i--;

			}


		}
	}



	public void ToggleNeighbors(bool b)
	{
		foreach (Node n in neighbors)
			n.ToggleEnabled(b);

		
	}

	void ToggleEnabled()
	{
		ToggleEnabled(!enabled);

	}

	//sets renderer and enabled variable
	public void ToggleEnabled(bool e)
	{
		enabled = e;
		transform.FindChild ("TriggerHolder/WaypointTrigger").GetComponent<Renderer> ().enabled = enabled;
		transform.FindChild ("TextHolder/Text").GetComponent<MeshRenderer> ().enabled = enabled;
		transform.FindChild ("GlowCylBase/GlowCyl").GetComponent<MeshRenderer> ().enabled = enabled;

	}
	//toggles the glow animation, utalizes iTween to change alpha of game object
	public void ToggleGlow(bool e)
	{
//		transform.FindChild ("Glow").GetComponent<SpriteRenderer> ().enabled = e;

		//make sure glow isnt animating before telling it to, prevents resetting animation
		if (!animating) {
			//either fade glow in, or fade glow out depending on input
			if (e) {
				iTween.FadeTo (transform.FindChild ("Glow").gameObject, iTween.Hash("alpha", 1, "time", 1, "onstart", "startAnimating", "oncomplete", "stopAnimating"));
				iTween.MoveTo (transform.FindChild ("GlowCylBase").gameObject, iTween.Hash("position", transform.position, "time", .5f, "easetype", "linear"));
				iTween.ScaleTo (transform.FindChild ("TriggerHolder/WaypointTrigger").gameObject, new Vector3 (.8f, .55f, .8f), .4f);
				iTween.FadeTo (transform.FindChild ("TextHolder").gameObject, iTween.Hash("alpha", 1, "time", .5, "onstart", "startAnimating", "oncomplete", "stopAnimating"));

				//moveTriggerAway (head.transform.position);

			} 
			if (!e) {
				iTween.FadeTo (transform.FindChild ("Glow").gameObject, iTween.Hash ("alpha", 0, "time", 1, "onstart", "startAnimating", "oncomplete", "stopAnimating"));
				iTween.MoveTo (transform.FindChild ("GlowCylBase").gameObject, iTween.Hash("position", new Vector3(transform.position.x, transform.position.y - 6.1f, transform.position.z), "time", .5f, "easetype", "linear"));
				iTween.ScaleTo (transform.FindChild ("TriggerHolder/WaypointTrigger").gameObject, new Vector3 (.36f, .24f, .36f), .4f);
				//moveTriggerToOrigin ();
				iTween.FadeTo (transform.FindChild ("TextHolder").gameObject, iTween.Hash("alpha", 0, "time", .5, "onstart", "startAnimating", "oncomplete", "stopAnimating"));


			} 
		}
		
	}
	public void ToggleLight (bool b)
	{
		if (b && !lightOn) 
		{
			iTween.ValueTo (gameObject, iTween.Hash ("from", 0, "to", lightIntensity, "time", 1f, "onupdate", "TweenLightUpdate"));
			lightOn = true;
		}
		if (!b && lightOn) 
		{
			iTween.ValueTo (gameObject, iTween.Hash ("from", lightIntensity, "to", 0, "time", 1f, "onupdate", "TweenLightUpdate"));
			lightOn = false;
		}
	}
	public void TweenLightUpdate(float f)
	{
		//transform.FindChild ("Spotlight").GetComponent<Light> ().intensity = f;
		//Debug.Log(f);
	}

	//function to hide glow effect instantaniously
	public void hide()
	{
		transform.FindChild ("TextHolder/Text").GetComponent<MeshRenderer> ().enabled = false;
		iTween.FadeTo (transform.FindChild ("Glow").gameObject, iTween.Hash("alpha", 0, "time", .01f, "onstart", "startAnimating", "oncomplete", "stopAnimating"));
		iTween.MoveTo (transform.FindChild ("GlowCylBase").gameObject, iTween.Hash("position", new Vector3(transform.position.x, transform.position.y - 6.1f, transform.position.z), "time", .001f, "easetype", "linear"));
	}
	public void stopAnimating()
	{
		animating = false;
	}
	public void startAnimating() 
	{
		animating = true;
	}
	public void moveTriggerAway (Vector3 moveAwayFrom)
	{
		//calc angle
		if (!movedTrigger) {
			originalTriggerPos = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			Vector3 moveVector = new Vector3 (originalTriggerPos.x - moveAwayFrom.x, originalTriggerPos.y, originalTriggerPos.z - moveAwayFrom.z);

			moveVector.Normalize ();
			moveVector.y = 0;
			moveVector *= 2;

			Vector3 destVector = transform.FindChild ("WaypointTrigger").transform.position + moveVector;
			iTween.MoveTo (transform.FindChild ("WaypointTrigger").gameObject, iTween.Hash("position", destVector, "time", .2f, "easetype", "linear"));
			//transform.FindChild ("WaypointTrigger").transform.position += moveVector;
			movedTrigger = true;
			//Debug.Log ("away");
		}
	}
	public void moveTriggerToOrigin()
	{
		if (movedTrigger) 
		{
			Transform wpt = transform.FindChild ("WaypointTrigger").transform;
			wpt.position = new Vector3 (transform.position.x, wpt.position.y, transform.position.z);
			movedTrigger = false;
			//Debug.Log ("origin");
		}
	}

	public void TweenProgressUpdate(float f)
	{
		float fill = transform.FindChild ("TriggerHolder/WaypointTrigger/Canvas/Progress").GetComponent<Image> ().fillAmount;
		if (fill < 1) 
			fill += f;
		if (fill >= 1) {
			fill = 1;
			hasLookedForTime = true;
		}
		
		transform.FindChild ("TriggerHolder/WaypointTrigger/Canvas/Progress").GetComponent<Image> ().fillAmount = fill;
		//Debug.Log(f);
	}
	public void stopProgress()
	{
		lookingAtWP = false;
		hasLookedForTime = false;
	}





}
