﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Triceratops_Grazing : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        CreateSequence();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void CreateSequence()
    {
        Sequence triceratopsSequence = DOTween.Sequence();
        triceratopsSequence.Append(transform.DOLookAt(new Vector3(Random.Range(-50, 50), 0f, Random.Range(-50, 50)), Random.Range(0, 8)).SetEase(Ease.InOutCubic).SetDelay(Random.Range(0, 8)));
        triceratopsSequence.OnComplete(CreateSequence);
    }
}
