﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Triceratops_Movement : MonoBehaviour
{
    Rigidbody rigBod;
    public Vector3 point1;
    public Vector3 point2;
    public Vector3 point3;
    bool animating = false;
    
    // Use this for initialization
    void Start()
    {
        rigBod = gameObject.GetComponent<Rigidbody>();
        //CreateSequence();
    }

    // Update is called once per frame
    void Update()
    {
        if (Cardboard.SDK.Triggered)
        {
            DOTween.Pause(this);
        }
    }
    public void CreateSequence()
    {
        Sequence triceratopsSequence = DOTween.Sequence();

        if (!animating)
        {
            animating = true;
            triceratopsSequence.Append(transform.DOLookAt(point1, 3f).SetEase(Ease.InOutCubic));//Look at player over after delay
            triceratopsSequence.Append(transform.DOMove(point1, 3f).SetEase(Ease.InOutCubic));//Charge player
            triceratopsSequence.Append(transform.DOLookAt(point2, 3f).SetEase(Ease.InOutCubic).SetDelay(Random.Range(5, 8)));//Look at start spot over after delay
            triceratopsSequence.Append(transform.DOMove(point2, 9f).SetEase(Ease.InOutCubic));//Move back to start
            triceratopsSequence.Append(transform.DOLookAt(point3, 4));//Look at graze point
            //triceratopsSequence.OnComplete(CreateSequence).SetDelay(Random.Range(0,5));//Start over after delay
            triceratopsSequence.OnComplete(AnimationEnded);
        }
        
       

    }
    void AnimationEnded()
    {
        animating = false;
    }
}
